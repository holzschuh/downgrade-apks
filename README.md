# DOWNGRADE APKS #

Micro tutorial para compatibilizar APKs com os tablets (android < 4.1)

### Requesitos ###

* APK Easy Tool [Baixar](https://androidfilehost.com/?w=files&flid=52342) | [Fórum Oficial](https://forum.xda-developers.com/android/software-hacking/tool-apk-easy-tool-v1-02-windows-gui-t3333960)
	- Windows 7 or above
	- .NET Framework 4.6.2 or above
	- Java SE/JDK for decompile, compile, and sign APK.
* APK do projeto

## Passo a passo ##

1. Instalar/descompactar o APK Easy Tool
2. Copiar o `APK` do projeto para uma pasta qualquer
3. Abrir a ferramenta e selecionar o APK
>![step1](https://i.ibb.co/y6pc2sR/stp1.png)
4. Clicar em '**Decompile**'
5. Aguardar até o processo finalizar
6. Abrir a pasta onde o APK foi descompilado (é só clicar no botão '**Decompiled APK Directory**')
>![step2](https://i.ibb.co/7p3rmHD/stp1.png)
7. Abrir o arquivo `apktool.yml` e editar a seguinte linha ```minSdkVersion: '15'```, substituindo o número que estiver nela por **15** e salvar o arquivo
>![step3](https://i.ibb.co/PzQk24W/stp1.png)
8. Abrir o arquivo `AndroidManifest.xml`, presente na mesma pasta
9. Remover os seguintes atributos, se existirem: (os números podem variar...)
	- `android:compileSdkVersion="28"`
	- `android:compileSdkVersionCodename="9"`
>![step4](https://i.ibb.co/7GK46Cx/stp1.png)
10. Salvar o arquivo e retornar ao APK Easy Tool
11. Clicar em '**Compile**'
>![step5](https://i.ibb.co/wdVHcmB/stp1.png)
12. Após finalizar o processo, a APK modificada estará na pasta '2-Recompiled APKs', acessível através do botão '**Compiled APK Directory**'

### Ajuda? ###
Se precisar de ajuda, chame no grupo.